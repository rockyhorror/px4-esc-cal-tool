# px4-esc-cal-tool

## Summary

The PWM ESC calibration process has limitations when using QGroundControl and PX4, namely you need to remove the battery to perform the calibration.

This tool allows you to get around this restriction, **assuming** you have access to remove the PWM output cable.

## Environment

This project requires python3 and pip3. To install required packages:

```shell
python3 -m pip install -r requirements.txt
```

## Usage

[A detailed guide can be found here](https://docs.modalai.com/flight-core-pwm-esc-calibration/)

 