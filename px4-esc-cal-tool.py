################################################################################
# Copyright 2020 ModalAI Inc.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice,
#    this list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#
# 3. Neither the name of the copyright holder nor the names of its contributors
#    may be used to endorse or promote products derived from this software
#    without specific prior written permission.
#
# 4. The Software is used solely in conjunction with devices provided by
#    ModalAI Inc.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
################################################################################

from os import system, sys, path
from argparse import ArgumentParser

if sys.version_info[0] < 3:
    raise Exception("Python 3 or a more recent version is required.")

try:
    from pymavlink import mavutil
    import serial
except:
    print("Failed to import pymavlink.")
    print("You may need to install it with 'pip install pymavlink pyserial'")
    print("")
    raise


class MavlinkShell:
    """
    Adapted from https://github.com/PX4/Firmware/blob/master/Tools/mavlink_shell.py
    """
    def __init__(self, portname, baud):
        """
        Constructor
        """
        self.mav = None
        self.portname = portname
        self.baud = baud
        # SERIAL_CONTROL_DEV_SHELL	= 10
        self.devnum = 10

    def connect(self):
        """
        Attempt connection over USB to PX4
        :return: True if connected
        """
        try:
            self.mav = mavutil.mavlink_connection(self.portname, autoreconnect=True, baud=self.baud)
        except:
            return False

        ret = self.mav.wait_heartbeat(blocking=True, timeout=10)
        return ret is not None

    def write(self, cmd):
        """
        Write over USB to PX4
        :param cmd: string command to write
        :return: none
        """
        flags = mavutil.mavlink.SERIAL_CONTROL_FLAG_RESPOND
        flags |= mavutil.mavlink.SERIAL_CONTROL_FLAG_MULTI
        flags |= mavutil.mavlink.SERIAL_CONTROL_FLAG_EXCLUSIVE

        while len(cmd) > 0:
            n = len(cmd)
            if n > 70:
                n = 70
            buf = [ord(x) for x in cmd[:n]]
            buf.extend([0] * (70 - len(buf)))

            self.mav.mav.serial_control_send(self.devnum,
                                             flags,
                                             0,
                                             self.baud,
                                             n,
                                             buf)
            cmd = cmd[n:]


class UserInterface:
    """
    Helper class to handle CLI
    """
    def __init__(self):
        """
        Constructor
        """
        pass

    @staticmethod
    def display_warnings():
        system("clear")
        print("*******************************************************************")
        print(" WARNING!")
        print("")
        print("  This utility is used to bypass the QGroundControl safety and")
        print("  battery validation logic. This is done to allow PWM ESC")
        print("  calibration without removing the battery.")
        print("")
        print("  To proceed using this tool understanding the risks, enter 'y'")
        result = input("> ")
        if result is not 'y':
            print("cancelled by user")
            exit(0)
        system("clear")

        print("*******************************************************************")
        print(" WARNING!")
        print("")
        print("  REMOVE THE PROPELLERS BEFORE PROCEEDING!")
        print("")
        print("  After removing propellers, enter 'y'")
        result = input("> ")
        if result is not 'y':
            print("cancelled by user")
            exit(0)
        system("clear")

        print("*******************************************************************")
        print(" WARNING!")
        print("")
        print("  Disconnect the PWM output cable:")
        print("   - on VOXL Flight - J1007")
        print("   - on Flight Core - J7")
        print("")
        print(" Enter 'y' after disconnecting")
        result = input("> ")
        if result is not 'y':
            print("cancelled by user")
            exit(0)
        system("clear")

        print("  Ensure you have a USB connection to the flight controller:")
        print("   - VOXL Flight - J1006")
        print("   - Flight Core - J3")
        print("")
        print("  -- or--")
        print("")
        print("  Ensure have a UDP connection via voxl-vision-px4")
        print("")
        print("  Press 'y' to proceed")
        result = input("> ")
        if result is not 'y':
            print("cancelled by user")
            exit(0)
        system("clear")

        print("  Ensure the vehicle is powered by battery")
        print("")
        print(" Press 'y' to proceed")
        result = input("> ")
        if result is not 'y':
            print("cancelled by user")
            exit(0)
        system("clear")

    @staticmethod
    def display_pwm_jingle1():
        print("  Reconnect the PWM cable, you should hear a jingle.")
        print("")
        print("  Wait for the first calibration jingle to complete.")
        print("")
        print("  After the jingle stops, press [ENTER] to proceed")
        input("> ")
        system("clear")

    @staticmethod
    def display_pwm_jingle2():
        print("  You should hear a second jingle.")
        print("")
        print("  Wait for the second calibration jingle to complete.")
        print("")
        print("  After the second jingle stops, press [ENTER] to proceed")
        input("> ")
        system("clear")
        print("PWM ESC calibration complete!")


def mavcal():
    """
    Adapted from https://github.com/PX4/Firmware/blob/master/Tools/mavlink_shell.py
    :return:
    """
    parser = ArgumentParser(description=__doc__)
    parser.add_argument('port', metavar='PORT', nargs='?', default=None,
                        help='Mavlink port name: serial: DEVICE[,BAUD], udp: IP:PORT, tcp: tcp:IP:PORT. Eg: \
                                /dev/ttyUSB0 or 0.0.0.0:14550. Auto-detect serial if not given.')
    parser.add_argument("--baudrate", "-b", dest="baudrate", type=int,
                        help="Mavlink port baud rate (default=57600)", default=57600)
    args = parser.parse_args()

    if args.port is None:
        if sys.platform == "darwin":
            if path.exists("/dev/tty.usbmodem1"):
                args.port = "/dev/tty.usbmodem1"
            elif path.exists("/dev/tty.usbmodem01"):
                args.port = "/dev/tty.usbmodem01"
            else:
                print("Error: no serial connection found")
                return
        else:
            serial_list = mavutil.auto_detect_serial(preferred_list=['*FTDI*',
                                                                     "*Arduino_Mega_2560*", "*3D_Robotics*",
                                                                     "*USB_to_UART*", '*PX4*', '*FMU*', "*Gumstix*"])

            if len(serial_list) == 0:
                print("Error: no serial connection found")
                return

            if len(serial_list) > 1:
                print('Auto-detected serial ports are:')
                for port in serial_list:
                    print(" {:}".format(port))
            args.port = serial_list[0].device

    print('Using portname {:}'.format(args.port))

    cli = UserInterface()
    cli.display_warnings()

    print("connecting...")
    mavshell = MavlinkShell(args.port, args.baudrate)
    if not mavshell.connect():
        print("Error: Unable to open port")
    else:
        print("connected")
        mavshell.write('\n')

        mavshell.write("fw_att_control stop\n")
        # v1.10
        mavshell.write("mc_att_control stop\n")
        # v1.11
        mavshell.write("mc_rate_control stop\n")

        mavshell.write("esc_calib -a\n")
        mavshell.write("y")

        cli.display_pwm_jingle1()

        mavshell.write("\r\n")

        cli.display_pwm_jingle2()


if __name__ == '__main__':
    mavcal()
